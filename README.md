# Tinusaur Board 3 Documentation

## Tinusaur Board 3 STD

![Tinusaur Board 3 STD Assembling Guide](guide_thumb.jpg "Tinusaur Board 3 STD Assembling Guide")

- Download link: https://gitlab.com/tinusaur/board-3-docs/-/raw/master/Tinusaur%20Board%203%20-%20Assembling.pdf?inline=false

## Tinusaur Board 3 LTA

![Tinusaur Board 3 LTA Assembling Guide](guide_lta_thumb.jpg "Tinusaur Board 3 LTA Assembling Guide")

- Download link: https://gitlab.com/tinusaur/board-3-docs/-/raw/master/Tinusaur_Board_3_LTA_Assembling_Guide.pdf.pdf?inline=false

